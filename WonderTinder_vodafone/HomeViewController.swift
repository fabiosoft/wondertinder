//
//  HomeViewController.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import UIKit

enum WonderTinderSection {
    case main
}

class HomeViewController: UIViewController {
    typealias WonderTinderDataSource = UICollectionViewDiffableDataSource<WonderTinderSection, WonderTinderViewModel>
    typealias Snapshot = NSDiffableDataSourceSnapshot<WonderTinderSection, WonderTinderViewModel>
    
    private var homeViewModel: HomeViewModel!
    weak var coordinator: MainCoordinator?
    
    private lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
//        self.collectionView.dataSource = self
        cv.delegate = self
        cv.isPagingEnabled = true
        cv.register(CharacterCell.self, forCellWithReuseIdentifier: CharacterCell.reuseIdentifier)
        return cv
    }()
    
    private var storage : MemoryStorage!

    init(viewModel: HomeViewModel? = HomeViewModel(), storage: MemoryStorage = MemoryStorage.shared) {
        super.init(nibName: nil, bundle: nil)
        self.homeViewModel = viewModel
        self.storage = storage
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()

    }
    
    private lazy var dataSource = makeDataSource()
    func makeDataSource() -> WonderTinderDataSource {
        let dataSource = WonderTinderDataSource(
            collectionView: self.collectionView,
            cellProvider: { collectionView, indexPath, wonderTinderElement in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterCell.reuseIdentifier, for: indexPath) as! CharacterCell
                cell.contentView.backgroundColor = UIColor.random()
                cell.model = wonderTinderElement
                cell.delegate = self
                return cell
            })
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems([])
        dataSource.apply(snapshot, animatingDifferences: false)
        return dataSource
    }
    
    func applySnapshot(items: [WonderTinderViewModel], animatingDifferences: Bool = false) {
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(items)
        self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    
    @objc private func openLikes(){
        self.coordinator?.openLikes(storage: self.storage)
    }
    
    private func setupViews() {
        self.title = homeViewModel.title
        self.view.backgroundColor = .systemBackground
        let likesButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(openLikes))
        self.navigationItem.rightBarButtonItem = likesButton
        
        self.view.addSubview(self.collectionView)
        NSLayoutConstraint.activate([
            self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchItems()
        
    }
    
    private func fetchItems() {
        homeViewModel.fetchCharacters { [weak self] wonderTinders in
            guard let self = self else {
                return
            }
            self.applySnapshot(items: wonderTinders)
            if(wonderTinders.count > 0){
                self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .bottom, animated: false)
            }
        }
    }
}

extension HomeViewController : UICollectionViewDelegate {
    
}
extension HomeViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}


extension HomeViewController : CharacterCellDelegate {
    func swipedLeft(_ sender: WonderTinderViewModel?) {
        guard let sender = sender else {
            return
        }
        self.coordinator?.showDislikedAlert(character: sender)
        self.storage.removeCharacter(character: sender)
    }
    
    func swipedRight(_ sender: WonderTinderViewModel?) {
        guard let sender = sender else {
            return
        }
        self.coordinator?.showLikedAlert(character: sender)
        self.storage.addCharacter(character: sender)
    }
}
