//
//  UIColor+App.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import Foundation
import UIKit

extension UIColor {
    class var appRed: UIColor {
        return UIColor(red: 0.904, green: 0.003, blue: 0.001, alpha: 1.00)
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
