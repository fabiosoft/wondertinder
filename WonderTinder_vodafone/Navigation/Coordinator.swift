//
//  Coordinator.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set } // larger app, sub sections coordinators
    var navigationController: UINavigationController { get set }

    func start()
}

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let homeVC = HomeViewController(viewModel: HomeViewModel(service: MockNetworkClient()))
        homeVC.coordinator = self
        navigationController.pushViewController(homeVC, animated: false)
    }
    
    func showLikedAlert(character:WonderTinderViewModelProtocol?){
        let alert = UIAlertController(title: NSLocalizedString("Liked", comment: ""), message: NSLocalizedString("You liked \(character?.username ?? "")", comment: ""), preferredStyle: .alert)
        let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel)
        alert.addAction(ok)
        if let topVc = self.navigationController.viewControllers.last {
            topVc.present(alert, animated: true)
        }
    }
    
    func showDislikedAlert(character:WonderTinderViewModelProtocol?){
        let alert = UIAlertController(title: NSLocalizedString("Disliked", comment: ""), message: NSLocalizedString("You disliked \(character?.username ?? "")", comment: ""), preferredStyle: .alert)
        let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel)
        alert.addAction(ok)
        if let topVc = self.navigationController.viewControllers.last {
            topVc.present(alert, animated: true)
        }
    }
    
    func openLikes<S:Storage>(storage:S){
        let likesVC = LikesViewController(viewModel: LikesViewModel(), storage: MemoryStorage.shared)
        likesVC.coordinator = self
        navigationController.pushViewController(likesVC, animated: true)
    }
}
