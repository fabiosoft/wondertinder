//
//  NetworkClientMock.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import UIKit

class NetworkSessionTaskMock: NetworkSessionTask {

    private let closure: () -> Void

    init(closure: @escaping () -> Void) {
        self.closure = closure
    }

    func resume() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.closure()
        }
    }

    func cancel() {

    }
}

class MockNetworkClient: Network {
    
    private let session: NetworkSession

    private var avatarCache = NSCache<NSString, UIImage>()

    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }
    
    func characters(perPage: Int, offset: Int, completion: @escaping (Result<[WonderResult], NetworkError>) -> Void) {
        
        let fileURL = Bundle.main.url(forResource: "Characters_Mock", withExtension: "json")!
        let data = try! Data(contentsOf: fileURL)
        let root = try? JSONDecoder().decode(WonderTinder.self, from: data)
        
        guard let wonderRoot = root,
              let wonderTinderData = wonderRoot.data,
              let wonderTinderResults = wonderTinderData.results else {
            completion(.failure(.malformedData))
            return
        }
        completion(.success(wonderTinderResults))
    }
    
    func characterImage(_ wonderTinder: WonderResult, completion: @escaping (Result<ImageResult?, NetworkError>) -> Void) -> NetworkSessionTask? {
//        let uuid = wonderTinder.name
//        let task = NetworkSessionTaskMock {
//            completion(.success(
//                ImageResult(UIImage(named: "Characher_Mock_Image"), metadata: uuid)
//            ))
//        }
//        return task
        
        
        let uuid = wonderTinder.name
        guard let thumbObj = wonderTinder.thumbnail else {
            return nil
        }
        guard let imageURLString = thumbObj.path, let thumbnailExtension = thumbObj.thumbnailExtension else {
            return nil
        }
        let imageURL = URL(string: imageURLString.appending(".").appending(thumbnailExtension.rawValue))
        guard let imageURL = imageURL else {
            return nil
        }
        if let cachedImage = avatarCache.object(forKey: imageURLString as NSString) {
            let result = ImageResult(cachedImage, metadata: uuid)
            completion(.success(result))
            return nil
        }

        return session.loadData(from: imageURL) { data, _, error in
            guard let data = data, error == nil else {
                completion(.failure(.networkError))
                return
            }
            if data.isEmpty {
                completion(.failure(.malformedData))
                return
            }
            if let image = UIImage(data: data) {
                self.avatarCache.setObject(image, forKey: imageURLString as NSString)
                let result = ImageResult(image, metadata: uuid)
                completion(.success(result))
            } else {
                completion(.failure(.malformedData))
            }
        }
    }
}

