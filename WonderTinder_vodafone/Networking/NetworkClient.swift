//
//  NetworkClient.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import UIKit

protocol Network {
    func characters(perPage: Int, offset: Int, completion: @escaping (Result<[WonderResult], NetworkError>) -> Void)
    func characterImage(_ wonderTinder: WonderResult, completion: @escaping (Result<ImageResult?, NetworkError>) -> Void) -> NetworkSessionTask?
}

enum NetworkError: Error {
    case networkError
    case malformedURL
    case malformedData
}

class ImageResult: NSObject {
    var image: UIImage?
    var metadata: String?
    init(_ image: UIImage?, metadata: String?) {
        self.image = image
        self.metadata = metadata
    }
}

protocol NetworkSession {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask
    func loadData(from url: URLRequest,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask
}
protocol NetworkSessionTask {
    func resume()
    func cancel()
}

extension URLSessionDataTask: NetworkSessionTask {

}

extension URLSession: NetworkSession {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask {
        return dataTask(with: url) { (data, response, error) in
            completionHandler(data, response, error)
        }
    }
    func loadData(from url: URLRequest,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask{
        return dataTask(with: url) { (data, response, error) in
            completionHandler(data, response, error)
        }
    }
}

class NetworkClient: Network {
    private let session: NetworkSession

    private var avatarCache = NSCache<NSString, UIImage>()

    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }

    func characters(perPage: Int, offset: Int, completion: @escaping (Result<[WonderResult], NetworkError>) -> Void) {
        let urlString = "https://gateway.marvel.com/v1/public/characters"
        var comps = URLComponents(string: urlString)
        
        let limit = URLQueryItem(name: "limit", value: "\(perPage)")
        let apikey = URLQueryItem(name: "apikey", value: "71f4fcc243805428337115ef09cd37c5")
        comps?.queryItems = [limit, apikey]
        let url = comps?.url
        
        guard let url = url else {
            completion(.failure(.malformedURL))
            return
        }
        var request = URLRequest(url: url)
        request.addValue("Accept", forHTTPHeaderField: "application/json")
        request.addValue("https://developer.marvel.com/", forHTTPHeaderField: "Referer")
        let task = session.loadData(from: request) { data, _, error in
            guard let data = data, error == nil else {
                completion(.failure(.networkError))
                return
            }
            let root = try? JSONDecoder().decode(WonderTinder.self, from: data)
            guard let wonderRoot = root,
                  let wonderTinderData = wonderRoot.data,
                  let wonderTinderResults = wonderTinderData.results else {
                completion(.failure(.malformedData))
                return
            }
            completion(.success(wonderTinderResults))

        }
        task.resume()
    }
    
    

    func characterImage(_ wonderTinder: WonderResult, completion: @escaping (Result<ImageResult?, NetworkError>) -> Void) -> NetworkSessionTask?{
       
        let uuid = wonderTinder.name
        guard let thumbObj = wonderTinder.thumbnail else {
            return nil
        }
        guard let imageURLString = thumbObj.path, let thumbnailExtension = thumbObj.thumbnailExtension else {
            return nil
        }
        let imageURL = URL(string: imageURLString.appending(".").appending(thumbnailExtension.rawValue))
        guard let imageURL = imageURL else {
            return nil
        }
        if let cachedImage = avatarCache.object(forKey: imageURLString as NSString) {
            let result = ImageResult(cachedImage, metadata: uuid)
            completion(.success(result))
            return nil
        }

        return session.loadData(from: imageURL) { data, _, error in
            guard let data = data, error == nil else {
                completion(.failure(.networkError))
                return
            }
            if data.isEmpty {
                completion(.failure(.malformedData))
                return
            }
            if let image = UIImage(data: data) {
                self.avatarCache.setObject(image, forKey: imageURLString as NSString)
                let result = ImageResult(image, metadata: uuid)
                completion(.success(result))
            } else {
                completion(.failure(.malformedData))
            }
        }
    }
}
