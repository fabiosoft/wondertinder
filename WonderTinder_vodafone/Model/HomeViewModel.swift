//
//  HomeViewModel.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import Foundation
import UIKit

protocol HomeViewModelProtocol {
    var title: String { get }
    func fetchCharacters(completion: @escaping ([WonderTinderViewModel]) -> Void)
}

class HomeViewModel: HomeViewModelProtocol {
    private let service: Network!
    let title = "WonderTinder"
    private var isfetching = false
    private var currentPage = 1
    
    init(service: Network = NetworkClient()) {
        self.service = service
    }
    
    
    func fetchCharacters(completion: @escaping ([WonderTinderViewModel]) -> Void) {
        if isfetching {
            return
        }
        isfetching = true
        
        service.characters(perPage: 10, offset: 0) { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {

                case .success(let characters):
                    self.currentPage += 1
                    let viewmodels = characters.compactMap { WonderTinderViewModel($0, network: MockNetworkClient()) }
                    completion(viewmodels)
                case .failure:
                    completion([])
                }
                self.isfetching = false
            }
        }
    }
}
