//
//  Storage.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 26/03/22.
//

import Foundation

protocol Storage: AnyObject{
    associatedtype Item
    func allCharacters() -> [Item]
    func addCharacter(character: Item)
    func removeCharacter(character: Item) -> Item?
}


class MemoryStorage:Storage{
    typealias Item = WonderTinderViewModel
    
    static let shared = MemoryStorage()
    private var storage = Set<Item>()
    
    func allCharacters() -> [Item] {
        return Array(storage)
    }
    
    func addCharacter(character: Item) {
        storage.insert(character)
    }
    
    @discardableResult
    func removeCharacter(character: Item)->Item? {
        return storage.remove(character)
    }
    
    
}
