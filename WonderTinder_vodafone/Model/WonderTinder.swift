//
//  WonderTinder.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 24/03/22.
//

import Foundation
import UIKit

protocol WonderTinderViewModelProtocol {
    var username: String? {get}
    var avatar: URL? {get}
    var avatarPlaceholder: UIImage? {get}
    func thumbnailImage(_ completion: @escaping (ImageResult?) -> Void) -> NetworkSessionTask?
}

class WonderTinderViewModel: WonderTinderViewModelProtocol {
    
    private(set) var wonderTinderElement: WonderResult
    private var network: Network

    init(_ wonderTinderElement: WonderResult, network: Network = NetworkClient()) {
        self.wonderTinderElement = wonderTinderElement
        self.network = network
    }
    
    lazy var username: String? = {
        return self.wonderTinderElement.name
    }()

    lazy var avatar: URL? = {
        // usually image view frame is 340x210
        if let imagePath1 = wonderTinderElement.thumbnail?.path {
            return URL(string: imagePath1)
        }
        return nil
    }()

    lazy var avatarPlaceholder: UIImage? = {
        nil //UIImage.owner
    }()

    func thumbnailImage(_ completion: @escaping (ImageResult?) -> Void) -> NetworkSessionTask? {
        return self.network.characterImage(self.wonderTinderElement) { result in
            switch result {
            case .success(let imageResult):
                completion(imageResult)
            case .failure:
                completion(nil)
            }
        }
    }
    
}
extension WonderTinderViewModel: Hashable {
    static func == (lhs: WonderTinderViewModel, rhs: WonderTinderViewModel) -> Bool {
        return lhs.wonderTinderElement.id == rhs.wonderTinderElement.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(wonderTinderElement.id)
    }
}
extension WonderTinderViewModel: CustomStringConvertible, CustomDebugStringConvertible {
    var description: String {
        return wonderTinderElement.name ?? "\(String(describing: wonderTinderElement.id)), no name"
    }

    var debugDescription: String {
        return self.description
    }
}


class WonderTinder: Codable {
    let code: Int?
    let status, copyright, attributionText, attributionHTML: String?
    let etag: String?
    let data: DataClass?

    init(code: Int?, status: String?, copyright: String?, attributionText: String?, attributionHTML: String?, etag: String?, data: DataClass?) {
        self.code = code
        self.status = status
        self.copyright = copyright
        self.attributionText = attributionText
        self.attributionHTML = attributionHTML
        self.etag = etag
        self.data = data
    }
}

// MARK: - DataClass
class DataClass: Codable {
    let offset, limit, total, count: Int?
    let results: [WonderResult]?

    init(offset: Int?, limit: Int?, total: Int?, count: Int?, results: [WonderResult]?) {
        self.offset = offset
        self.limit = limit
        self.total = total
        self.count = count
        self.results = results
    }
}

// MARK: - Result
class WonderResult: Codable {
    let id: Int?
    let name, resultDescription: String?
    let modified: String?
    let thumbnail: Thumbnail?

    enum CodingKeys: String, CodingKey {
        case id, name
        case resultDescription = "description"
        case modified, thumbnail
    }

    init(id: Int?, name: String?, resultDescription: String?, modified: String?, thumbnail: Thumbnail?) {
        self.id = id
        self.name = name
        self.resultDescription = resultDescription
        self.modified = modified
        self.thumbnail = thumbnail
    }
}

class Thumbnail: Codable {
    let path: String?
    let thumbnailExtension: Extension?

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }

    init(path: String?, thumbnailExtension: Extension?) {
        self.path = path
        self.thumbnailExtension = thumbnailExtension
    }
}

enum Extension: String, Codable {
    case jpg = "jpg"
}
