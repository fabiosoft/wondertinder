//
//  LikesViewModel.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 27/03/22.
//

import Foundation

protocol LikesViewModelProtocol {
    var title: String { get }
}

class LikesViewModel : LikesViewModelProtocol {
    var title: String {
        return "Likes"
    }
}
