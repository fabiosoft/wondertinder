//
//  LikesViewController.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 27/03/22.
//

import Foundation
import UIKit

class LikesViewController: UIViewController {
    
    typealias WonderTinderLikesDataSource = UICollectionViewDiffableDataSource<WonderTinderSection, WonderTinderViewModel>
    typealias SnapshotLikes = NSDiffableDataSourceSnapshot<WonderTinderSection, WonderTinderViewModel>
    
    var likesViewModel : LikesViewModelProtocol!
    weak var coordinator: MainCoordinator?
    
    private lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
//        self.collectionView.dataSource = self
        cv.delegate = self
        cv.isPagingEnabled = true
        cv.register(CharacterCell.self, forCellWithReuseIdentifier: CharacterCell.reuseIdentifier)
        return cv
    }()
    
    private var storage : MemoryStorage!

    init(viewModel: LikesViewModelProtocol = LikesViewModel(), storage: MemoryStorage = MemoryStorage.shared) {
        super.init(nibName: nil, bundle: nil)
        self.likesViewModel = viewModel
        self.storage = storage
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()

    }
    
    private lazy var dataSource = makeDataSource()
    func makeDataSource() -> WonderTinderLikesDataSource {
        let dataSource = WonderTinderLikesDataSource(
            collectionView: self.collectionView,
            cellProvider: { collectionView, indexPath, wonderTinderElement in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterCell.reuseIdentifier, for: indexPath) as! CharacterCell
                cell.contentView.backgroundColor = UIColor.random()
                cell.model = wonderTinderElement
                return cell
            })
        var snapshot = SnapshotLikes()
        snapshot.appendSections([.main])
        snapshot.appendItems([])
        dataSource.apply(snapshot, animatingDifferences: false)
        return dataSource
    }
    
    func applySnapshot(items: [WonderTinderViewModel], animatingDifferences: Bool = false) {
        var snapshot = SnapshotLikes()
        snapshot.appendSections([.main])
        snapshot.appendItems(items)
        self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    
    private func setupViews() {
        self.title = likesViewModel.title
        self.view.backgroundColor = .systemBackground
        
        self.view.addSubview(self.collectionView)
        NSLayoutConstraint.activate([
            self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchItems()
        
    }
    
    private func fetchItems() {
        let characters = self.storage.allCharacters()
        self.applySnapshot(items: characters)
    }
}

extension LikesViewController : UICollectionViewDelegate {
    
}
extension LikesViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}
