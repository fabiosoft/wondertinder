//
//  CharacterCell.swift
//  WonderTinder_vodafone
//
//  Created by Fabio Nisci on 25/03/22.
//

import Foundation
import UIKit

protocol CharacterCellDelegate:AnyObject { //only classes can conform to this protocol, whereas structs or enums can't
    func swipedLeft(_ sender:WonderTinderViewModel?)
    func swipedRight(_ sender:WonderTinderViewModel?)
}

class CharacterCell: UICollectionViewCell, Reusable{
    static var reuseIdentifier: String = "CharacterCell"
    private var avatarTask: NetworkSessionTask?
    
    weak var delegate : CharacterCellDelegate? = nil
    
    var model: WonderTinderViewModel? {
        didSet {
            self.usernameLabel.text = model?.username
            let placeholder = model?.avatarPlaceholder
            self.avatarImageView.image = placeholder
            self.avatarTask = model?.thumbnailImage({ result in
                DispatchQueue.main.async {

                    guard let result = result else {
                        self.avatarImageView.image = placeholder
                        return
                    }
                    if self.model?.username == result.metadata {
                        self.avatarImageView.image = result.image
                    }
                }
            })
            self.avatarTask?.resume()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarTask?.cancel()
        self.usernameLabel.text = nil
        self.avatarImageView.image = nil
    }


    lazy var avatarImageView: UIImageView = {
        let avatar = UIImageView()
        avatar.contentMode = .scaleAspectFill
        avatar.translatesAutoresizingMaskIntoConstraints = false
        avatar.clipsToBounds = true
        return avatar
    }()

    lazy var usernameLabel: UILabel = {
        let username = UILabel()
        username.textColor = .white
        username.translatesAutoresizingMaskIntoConstraints = false
        username.numberOfLines = 2
        username.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        return username
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let gradientView = UIView(frame: .zero)
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        gradientView.backgroundColor = .black.withAlphaComponent(0.8)
        
        self.contentView.addSubview(avatarImageView)
        self.contentView.addSubview(gradientView)
        self.contentView.addSubview(usernameLabel)
        NSLayoutConstraint.activate([
            avatarImageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            avatarImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            avatarImageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            avatarImageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),

            usernameLabel.leadingAnchor.constraint(equalTo: self.contentView.readableContentGuide.leadingAnchor),
            usernameLabel.trailingAnchor.constraint(equalTo: self.contentView.readableContentGuide.trailingAnchor),
            usernameLabel.bottomAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.bottomAnchor, constant: -60),
            
            gradientView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            gradientView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            gradientView.topAnchor.constraint(equalTo: self.usernameLabel.topAnchor, constant: -40),
        ])
        setupGestures()
    }
    
    private func setupGestures(){
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft(_:)))
        swipeLeftGesture.direction = .left
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight(_:)))
        swipeRightGesture.direction = .right
        self.addGestureRecognizer(swipeLeftGesture)
        self.addGestureRecognizer(swipeRightGesture)
    }
    
    @objc private func swipeLeft(_ sender:UISwipeGestureRecognizer){
        self.delegate?.swipedLeft(self.model)
    }
    
    @objc private func swipeRight(_ sender:UISwipeGestureRecognizer){
        self.delegate?.swipedRight(self.model)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
