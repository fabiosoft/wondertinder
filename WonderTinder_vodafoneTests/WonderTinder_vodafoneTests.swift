//
//  WonderTinder_vodafoneTests.swift
//  WonderTinder_vodafoneTests
//
//  Created by Fabio Nisci on 24/03/22.
//

import XCTest
@testable import WonderTinder_vodafone

class WonderTinder_vodafoneTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTitleHome() {
        let homeVM = HomeViewModel()
        XCTAssertEqual(homeVM.title, "WonderTinder")
    }

    func testSinglefetchStargazer() {
        let exp = XCTestExpectation()
        let network = MockNetworkClient()
        let perPage = 10
        network.characters(perPage: perPage, offset: 0) { result in
            switch result {
            case .success(let characters):
                XCTAssertEqual(characters.count, perPage)
            case .failure(let error):
                XCTAssertNotNil(error)

                exp.fulfill()
            }
        }
    }

    func testViewModel() {
        let wonder = WonderResult(id: 123, name: "Wonder Test", resultDescription: nil, modified: nil, thumbnail: nil)
        let wonderVM = WonderTinderViewModel(wonder)
        XCTAssertEqual(wonderVM.username, "Wonder Test")
        XCTAssertNil(wonderVM.avatar)
    }
}
