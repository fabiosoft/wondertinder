//
//  WonderTinder_vodafoneUITests.swift
//  WonderTinder_vodafoneUITests
//
//  Created by Fabio Nisci on 24/03/22.
//

import XCTest

class WonderTinder_vodafoneUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_navButton_likes() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let rightNavBarButton = app.navigationBars.children(matching: .button).firstMatch
        XCTAssert(rightNavBarButton.exists)

    }
    
    func test_collectionViewsItems() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let cv_cells = app.collectionViews.cells //visible cells
        XCTAssert(cv_cells.count == 1)

    }
}
